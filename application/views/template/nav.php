
<div class="container">
    <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand" href="#">Rautapojat</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo site_url() . 'home/'; ?>">Home</a></li>
        
        
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
         <?php
                if ($this->session->has_userdata('kayttaja')) {
                ?>
                <li><a href="<?php print site_url() . 'galleria/kirjaudu_ulos'; ?>">Kirjaudu ulos</a></li>
                <?php
                }
                else {
                ?><li><a href="<?php echo site_url() . 'galleria/'; ?>">Kirjaudu</a></li>
                <?php
                }
                ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</div>