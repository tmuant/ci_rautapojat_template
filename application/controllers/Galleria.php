<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galleria extends CI_Controller {
    public function __construct() {
        parent::__construct();
        
        
        $this->load->model('galleria_model');
        $this->load->library('encrypt');
        $this->load->library('form_validation');
        
    }
    
    public function index() {
        
                
                
        
        $data['title'] = 'Hallintapaneeli';
        $this->load->view('template/nav');
        $this->load->view('template/header',$data);
        $this->load->view('galleria/kirjaudu_view');
        $this->load->view('template/footer');
    }
    
    public function kirjaudu() { 
        $this->form_validation->set_rules('tunnus','tunnus','required');
        $this->form_validation->set_rules('salasana','salasana','required|min_length[8]');
        $this->form_validation->set_rules('tunnus','tunnus','callback_tarkasta_kayttaja');
        
        if ($this->form_validation->run()===TRUE) { 
            redirect('home/index');
        }
        else {
            $data['title'] = 'Hallintapaneeli';
            $this->load->view('template/nav');
            $this->load->view('template/header',$data);
            $this->load->view('galleria/kirjaudu_view');
            $this->load->view('template/footer');
            }
    }
    
    public function tarkasta_kayttaja() {
        $tunnus = $this->input->post('tunnus');
        $salasana = $this->input->post('salasana');
        
        $kayttaja = $this->galleria_model->tarkasta_kayttaja($tunnus,$salasana);
        
        if ($kayttaja) {
            $this->session->set_userdata('kayttaja',$kayttaja);
            return TRUE;
        }
        else {
            $this->form_validation->set_message('tarkasta_kayttaja','Käyttäjätunnus tai salasana virheellinen!');
            return FALSE;
        }
    }
    
    
    
    public function rekisteroityminen() {
        
        $data['title'] = 'Hallintapaneeli';
        $this->load->view('template/nav');
        $this->load->view('template/header',$data);
        $this->load->view('galleria/rekisteroidy_view');
        $this->load->view('template/footer');
    }   
    
    public function rekisteroidy() {
        $data = array(            
            'tunnus' => $this->input->post('tunnus'),
            'salasana' => $this->encrypt->encode($this->input->post('salasana'))
        );
        
        $this->form_validation->set_rules('tunnus','tunnus','required');
        $this->form_validation->set_rules('salasana','salasana','required|min_length[8]');
        $this->form_validation->set_rules('salasana2','salasana uudestaan','matches[salasana]');
        
        
        if ($this->form_validation->run()===TRUE) {                
            $this->galleria_model->lisaa($data);
            $this->index();
        }
        else {
        $data['title'] = 'Hallintapaneeli';
        $this->load->view('template/nav');
        $this->load->view('template/header',$data);
        $this->load->view('galleria/rekisteroidy_view');
        $this->load->view('template/footer');
        }
    }
    
    public function kirjaudu_ulos() {
        $this->session->unset_userdata('kayttaja');
        $this->session->sess_destroy();
        
        redirect('home','refresh');
    }
    
}












